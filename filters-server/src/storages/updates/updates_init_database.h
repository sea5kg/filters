#ifndef UPDATES_INIT_DATABASE_H
#define UPDATES_INIT_DATABASE_H

#include <storages.h>

class UpdatesInitDatabase : public StorageUpdateBase {
    public:
        UpdatesInitDatabase();
};

#endif // UPDATES_INIT_DATABASE_H
