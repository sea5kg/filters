#ifndef UNIT_TEST_STORAGE_ESCAPES_H
#define UNIT_TEST_STORAGE_ESCAPES_H

#include <unit_tests.h>

class UnitTestStorageEscapes : public UnitTestBase {
    public:
        UnitTestStorageEscapes();
        virtual void init();
        virtual bool run();
};

#endif // UNIT_TEST_STORAGE_ESCAPES_H
