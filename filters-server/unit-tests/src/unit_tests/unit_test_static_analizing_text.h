#ifndef UNIT_TEST_STATIC_ANALIZING_TEXT_H
#define UNIT_TEST_STATIC_ANALIZING_TEXT_H

#include <unit_tests.h>

class UnitTestStaticAnalizingText : public UnitTestBase {
    public:
        UnitTestStaticAnalizingText();
        virtual void init();
        virtual bool run();
};

#endif // UNIT_TEST_STATIC_ANALIZING_TEXT_H
