#ifndef UNIT_TEST_LEVENSHTEIN_H
#define UNIT_TEST_LEVENSHTEIN_H

#include <unit_tests.h>

class UnitTestLevenshtein : public UnitTestBase {
    public:
        UnitTestLevenshtein();
        virtual void init();
        virtual bool run();
};

#endif // UNIT_TEST_LEVENSHTEIN_H
