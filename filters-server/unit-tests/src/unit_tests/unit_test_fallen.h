#ifndef UNIT_TEST_FALLEN_H
#define UNIT_TEST_FALLEN_H

#include <unit_tests.h>

class UnitTestFallen : public UnitTestBase {
    public:
        UnitTestFallen();
        virtual void init();
        virtual bool run();
};

#endif // UNIT_TEST_FALLEN_H
